import React, { useEffect, useState } from 'react'

function LocationForm (props) {

    
    const [locations, setLocations] = useState([])
    
    const [name, setName] = useState('')
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const [starts, setStarts] = useState('')
    const handleStartsChange = (event) => {
        const value = event.target.value
        setStarts(value)
    }
    
    const [ends, setEnds] = useState('')
    const handleEndsChange = (event) => {
        const value = event.target.value
        setEnds(value)
    }
    
    const [description, setDesc] = useState('')
    const handleDescChange = (event) => {
        const value = event.target.value
        setDesc(value)
    }

    const [max_presentations, setMaxP] = useState('')
    const handleMaxPChange = (event) => {
        const value = event.target.value
        setMaxP(value)
    }
    
    const [max_attendees, setMaxA] = useState('')
    const handleMaxAChange = (event) => {
        const value = event.target.value
        setMaxA(value)
    }

    const [location, setLocation] = useState('')
    const handleLocationChange = (event) => {
        const value = event.target.value
        setLocation(value)
    }
    
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'
        
        const response = await fetch(url)
        
        if (response.ok) {
            const data = await response.json()
            console.log(data.locations)
            setLocations(data.locations)
        }
    }
    
    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.name = name
        data.starts = starts
        data.ends = ends
        data.description = description
        data.max_presentations = max_p

        console.log(data)

        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            const newLocation = await response.json()
            console.log(newLocation)

            setName('')
            setStarts('')
            setEnds('')
            setDesc('')
            setMaxP('')
            setMaxA('')
            setLocation('')
        }
    }
    useEffect(() => {
        fetchData()
      }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange}  value={starts} placeholder="Starts" required type="date" name="starts" id="starts" class="form-control"></input>
                <label for="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange}  value={ends} placeholder="Ends" required type="date" name="ends" id="ends" class="form-control"></input>
                <label htmlFor="city">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDescChange}  value={description} placeholder="Description" required type="textarea" name="description" id="description" class="form-control"></input>
                <label htmlFor="city">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPChange}  value={max_presentations} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" class="form-control"></input>
                <label htmlFor="city">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAChange}  value={max_attendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" class="form-control"></input>
                <label htmlFor="city">Max Attendees</label>
              </div>
              <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required id="location"  name="location" class="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default LocationForm