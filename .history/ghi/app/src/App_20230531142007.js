import Nav from './Nav'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendeeForm from './AttendeeForm'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

function App(props) {
  return (
    <>
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path='attendees'>
          <Route index element={<AttendeesList/>}></Route>
          <Route path='new' element={<AttendeeForm />}></Route>
        </Route>
        <Route path="conferences">
          <Route index element={}></Route>
          <Route path="new" element={<ConferenceForm />}></Route>
        </Route>
        <Route path="locations/new"
        
        
        <LocationForm />
      </Routes>
    </BrowserRouter>
    </>
  );
}

export default App;