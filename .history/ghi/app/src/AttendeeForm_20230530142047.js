import React, { useEffect, useState } from 'react'

function AttendeeForm (props) {

    
    const [conferences, setConferences] = useState([])
    
    const [name, setName] = useState('')
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const [email, setEmail] = useState('')
    const handleEmailChange = (event) => {
        const value = event.target.value
        setEmail(value)
    }
    
    const [company, setCompany] = useState('')
    const handleCompanyChange = (event) => {
        const value = event.target.value
        setCompany(value)
    }
    
    const [conference, setConference] = useState('')
    const handleConferenceChange = (event) => {
        const value = event.target.value
        setConference(value)
    }
    
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/attendees/'
        
        const response = await fetch(url)
        
        if (response.ok) {
            const data = await response.json()
            console.log(data.conferences)
            setConferences(data.conferences)
        }
    }
    
    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.name = name
        data.email = email
        data.company_name = company
        data.conference = conference

        console.log(data)

        const attendeesUrl = 'http://localhost:8000/api/attendees/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(attendeesUrl, fetchConfig)
        if (response.ok) {
            const newAttendee = await response.json()
            console.log(newAttendee)

            setName('')
            setEmail('')
            setCompany('')
            setConference('')
        }
    }
    useEffect(() => {
        fetchData()
      }, [])

    return (
          <div class="container">
    <div class="my-5">
      <div class="row">
        <div class="col col-sm-auto">
          <img width="300" class="bg-white rounded shadow d-block mx-auto mb-4" src="./images/logo.svg">
        </div>
        <div class="col">
          <div class="card shadow">
            <div class="card-body">
              <form id="create-attendee-form">
                <h1 class="card-title">It's Conference Time!</h1>
                <p class="mb-3">
                  Please choose which conference
                  you'd like to attend.
                </p>
                <div class="d-flex justify-content-center mb-3" id="loading-conference-spinner">
                  <div class="spinner-grow text-secondary" role="status">
                    <span class="visually-hidden">Loading...</span>
                  </div>
                </div>
                <div class="mb-3">
                  <select name="conference" id="conference" class="form-select d-none" required>
                    <option value="">Choose a conference</option>
                  </select>
                </div>
                <p class="mb-3">
                  Now, tell us about yourself.
                </p>
                <div class="row">
                  <div class="col">
                    <div class="form-floating mb-3">
                      <input required placeholder="Your full name" type="text" id="name" name="name" class="form-control">
                      <label for="name">Your full name</label>
                    </div>
                  </div>
                  <div class="col">
                    <div class="form-floating mb-3">
                      <input required placeholder="Your email address" type="email" id="email" name="email" class="form-control">
                      <label for="email">Your email address</label>
                    </div>
                  </div>
                </div>
                <button class="btn btn-lg btn-primary">I'm going!</button>
              </form>
              <div class="alert alert-success d-none mb-0" id="success-message">
                Congratulations! You're all signed up!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange}  value={starts} placeholder="Starts" required type="date" name="starts" id="starts" className="form-control"></input>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleEndsChange}  value={ends} placeholder="Ends" required type="date" name="ends" id="ends" className="form-control"></input>
                <label htmlFor="city">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleDescChange}  value={description} placeholder="Description" required type="textarea" name="description" id="description" className="form-control"></input>
                <label htmlFor="city">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxPChange}  value={max_presentations} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control"></input>
                <label htmlFor="city">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleMaxAChange}  value={max_attendees} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control"></input>
                <label htmlFor="city">Max Attendees</label>
              </div>
              <div className="mb-3">
              <select onChange={handleLocationChange} value={location} required id="location"  name="location" className="form-select">
                <option value="">Choose a location</option>
                {locations.map(location => {
                    return (
                    <option key={location.id} value={location.id}>
                        {location.name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default AttendeeForm