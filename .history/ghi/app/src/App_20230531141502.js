import Nav from './Nav'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendeeForm from './AttendeeForm'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

function App(props) {
  return (
    <>
    <BrowserRouter>
      <Nav />
      <Routes></Routes>
        <AttendeeForm />
        <ConferenceForm />
        <LocationForm />
        <AttendeesList />
    </BrowserRouter>
    </>
  );
}

export default App;