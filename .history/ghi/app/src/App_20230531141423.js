import Nav from './Nav'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendeeForm from './AttendeeForm'
import { BrowserRouter, route, routes } from 'react-router-dom'

function App(props) {
  return (
    <>
    <browser
      <Nav />
        <AttendeeForm />
        <ConferenceForm />
        <LocationForm />
        <AttendeesList />
    </>
  );
}

export default App;