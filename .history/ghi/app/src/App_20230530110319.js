import Nav from './Nav'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'

function App(prop) {
  if (prop.attendees === undefined) {
    return null
  }
  return (
    <>
      <Nav />
      <div className="container">
        <LocationForm />
        <AttendeesList attendees={prop.attendees} />
      </div>
    </>
  );
}

export default App;