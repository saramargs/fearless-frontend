import React, { useEffect, useState } from 'react'

function LocationForm (props) {

    
    const [states, setStates] = useState([])
    
    const [state, setState] = useState('')
    const handleStateChange = (event) => {
        const value = event.target.value
        setState(value)
    }
    
    const [name, setName] = useState('')
    const handleNameChange = (event) => {
        const value = event.target.value
        setName(value)
    }

    const [city, setCity] = useState('')
    const handleCityChange = (event) => {
        const value = event.target.value
        setCity(value)
    }
    
    const [rooms, setRooms] = useState('')
    const handleRoomChange = (event) => {
        const value = event.target.value
        setRooms(value)
    }
    
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/'
        
        const response = await fetch(url)
        
        if (response.ok) {
            const data = await response.json()
            setStates(data.states)
        }
    }
    
    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {}

        data.room_count = rooms
        data.name = name
        data.city = city
        data.state = state

        console.log(data)

        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }

        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            const newLocation = await response.json()
            console.log(newLocation)

            setName('')
            setRooms('')
            setCity('')
            setState('')
        }
    }
    useEffect(() => {
        fetchData()
      }, [])

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new conference</h1>
            <form onSubmit={handleSubmit} id="create-location-form">
              <div className="form-floating mb-3">
                <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"></input>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleStartsChange}  value={starts} placeholder="Starts" required type="date" name="starts" id="starts" class="form-control"></input>
                <label for="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleCityChange}  value={city} placeholder="City" required type="text" name="city" id="city" className="form-control"></input>
                <label htmlFor="city">City</label>
              </div>
              <div className="mb-3">
              <select onChange={handleStateChange} value={state} required name="state" id="state" className="form-select">
                <option value="">Choose a state</option>
                {states.map(state => {
                    return (
                    <option key={state.abbreviation} value={state.abbreviation}>
                        {state.name}
                    </option>
                    );
                })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default LocationForm