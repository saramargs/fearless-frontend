import Nav from './Nav'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendeeForm from './AttendeeForm'
import { BrowserRouter, Route, Routes } from 'react-router-dom'

function App(props) {
  return (
    <>
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route path='/'></Route>
        <AttendeeForm />
        <ConferenceForm />
        <LocationForm />
        <AttendeesList />
      </Routes>
    </BrowserRouter>
    </>
  );
}

export default App;