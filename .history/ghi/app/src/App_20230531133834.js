import Nav from './Nav'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendeeForm from './AttendeeForm'

function App(props) {
  return (
    <>
      <Nav />
      <div className="container">
        <AttendeeForm />
        <ConferenceForm />
        <LocationForm />
        <AttendeesList />

      </div>
    </>
  );
}

export default App;