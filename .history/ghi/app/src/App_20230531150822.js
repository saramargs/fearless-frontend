import Nav from './Nav'
import AttendeesList from './AttendeesList'
import LocationForm from './LocationForm'
import ConferenceForm from './ConferenceForm'
import AttendeeForm from './AttendeeForm'
import PresentationForm from './PresentationForm'
import MainPage from "./MainPage"
import { BrowserRouter, Route, Routes } from 'react-router-dom'

function App(props) {
  return (
    <>
    <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path='attendees'>
          <Route index element={<AttendeesList/>}></Route>
          <Route path='new' element={<AttendeeForm />}></Route>
        </Route>
        <Route path="conferences/new" element={<ConferenceForm />}></Route>
        <Route path="conferences"></Route>
        <Route path="locations/new" element={<LocationForm />}></Route>
        <Route path="presentations/new" element={<PresentationForm />}></Route>
      </Routes>
    </BrowserRouter>
    </>
  );
}

export default App;