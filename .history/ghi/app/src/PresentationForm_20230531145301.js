import React, { useState, useEffect} from "react";

function PresentationForm() {

    const [conferences, setConferences] = useState([]);

    const [name, setName] = useState('');
    function handleNameChange(event) {
        setName(event.target.value)
    }

    const [email, setEmail] = useState('');
    function handleEmailChange(event) {
        setEmail(event.target.value)
    }

    const [companyName, setCompanyName] = useState("");
    function handleCompanyNameChange(event) {
        setCompanyName(event.target.value)
    }

    const [title, setTitle] = useState("");
    function handleTitleChange(event) {
        setTitle(event.target.value)
    }

    const [synopsis, setSynopsis] = useState("");
    function handleSynopsisChange(event) {
        setSynopsis(event.target.value)
    }

    const [conference, setConference] = useState("");
    function handleConferenceChange(event) {
        setConference(event.target.value)
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const data = {};
        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = companyName;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        const conferenceUrl = `http://localhost:8000/api/conferences/${conference}/presentations/`;
        const fetchConfig = {
          method : "post",
          body : JSON.stringify(data),
          headers: {
            'Content-type' : 'application/json',
          },
        };

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
          const newPresentation = await response.json();

          setEmail('');
          setName('');
          setTitle('');
          setConference(0);
          setSynopsis('');
          setCompanyName('');
        }
      }

    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new presentation</h1>
            <form onSubmit={ handleSubmit } id="create-presentation-form">
              <div className="form-floating mb-3">
                <input value= { name } onChange= { handleNameChange } placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control"></input>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input value= { email } onChange= { handleEmailChange } placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control"></input>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input value= { companyName } onChange= { handleCompanyNameChange } placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control"></input>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input value= { title } onChange= { handleTitleChange } placeholder="Title" required type="text" name="title" id="title" className="form-control"></input>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea value= { synopsis } onChange= { handleSynopsisChange } className="form-control" id="synopsis" rows="3" name="synopsis"></textarea>
              </div>
              <div className="mb-3">
                <select value= { conference } onChange= { handleConferenceChange } required name="conference" id="conference" className="form-select">
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option key={ conference.id } value={ conference.id }>{ conference.name }</option>
                    );
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    </div>
    )
};

export default PresentationForm;
