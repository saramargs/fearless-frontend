import React, { useEffect, useState } from 'react';

function AttendeesList() {

  const [attendees, setAttendees] = useState([]);

  const fetchData = async () => {
    const url = 'http://localhost:8001/api/attendees/';
    const response = await fetch(url);
    if (response.ok) {
        const data = await response.json();
        setAttendees(data.attendees);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

    return (
    <>
    <table className="table table-hover table-secondary table-striped border border-dark-subtle shadow container-fluid mt-5">
      <thead className="table-group-divider">
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody className="border-top border-dark-subtle">
        {attendees.map(attendee => {
          return (
          <tr className="object-fit" key={attendee.href }>
            <td>{ attendee.name }</td>
            <td>{ attendee.conference }</td>
          </tr>
        );
        })}
      </tbody>
    </table>
    </>);
  }

  export default AttendeesList;