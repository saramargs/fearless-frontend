window.addEventListener('DOMContentLoaded', async () =>{
    const locationUrl = 'http://localhost:8000/api/locations/'

    try {
        const response = await fetch(stateURL)
        const stateSelect = document.getElementById('location')
        
        if (!response.ok) {
            throw new Error("Bad response from API")
        } else {
            const data = await response.json()
            console.log(data)
            for (let location of data.locations){
                const option = document.createElement('option')
                option.value = location.id
                option.innerHTML = location.name
                
                stateSelect.appendChild(option)

            }
        }
    } catch (e) {
        console.error(e)

    }

    const formTag = document.getElementById('create-conference-form')
    formTag.addEventListener('submit', async event =>{
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const newConference = await response.json()
        }
    })
})
