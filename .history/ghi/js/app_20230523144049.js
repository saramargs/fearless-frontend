function createCard(name, description, pictureUrl) {
    return `
      <div class="card">
        <img src="{pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">{name}</h5>
          <p class="card-text">{description}</p>
        </div>
      </div>
    `;
  }

window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        console.log("Bad Response: ", response)
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();

        for (let conference of data.conferences) {
            const detailUrl = `http://localhost:8000${conference.href}`
            const detailResponse = await fetch(detailUrl)
            if (detailResponse.ok) {
                const details = await detailResponse.json()
                const title = details.conference.title
                const description = details.conference.description
                const pictureUrl = details.conference.picture_url
                const
            }
        }
  
        const conference = data.conferences[0];
        console.log(conference);
        const nameTag = document.querySelector('.card-title')
        nameTag.innerHTML = conference.name;

        const detailUrl = `http://localhost:8000${conference.href}`
        const detailResponse = await fetch(detailUrl)
        if (detailResponse.ok) {
            const details = await detailResponse.json()
            console.log(details)
            const detailsTag = document.querySelector('.card-text')
            detailsTag.innerHTML = details.conference.description
            const imgTag = document.querySelector('.card-img-top')
            imgTag.src = details.conference.location.picture_url
        }
  
      }
    } catch (e) {
        console.log("error")
      // Figure out what to do if an error is raised
    }
  
  });