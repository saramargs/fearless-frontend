window.addEventListener('DOMContentLoaded', async () =>{
    const stateURL = 'http://localhost:8000/api/states/'

    try {
        const response = await fetch(stateURL)
        
        if (!response.ok) {
            throw new Error("Bad response fromAPI")
        } else {
            const data = await response.json()
            console.log(data)
            
        }
    }
})