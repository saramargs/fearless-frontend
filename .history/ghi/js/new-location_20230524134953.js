window.addEventListener('DOMContentLoaded', async () =>{
    const stateURL = 'http://localhost:8000/api/states/'

    try {
        const response = await fetch(stateURL)
        const stateSelect = document.getElementById('state')
        
        if (!response.ok) {
            throw new Error("Bad response from API")
        } else {
            const data = await response.json()
            console.log(data)
            for (let state of data.states){
                const option = document.createElement('option')
                option.value = state.abbreviation
                option.innerHTML = state.name
                
                stateSelect.appendChild(option)

            }
        }
    } catch (e) {
        console.error(e)

    }

    const formTag = document.getElementById('create-location-form')
    formTag.addEventListener('submit', event =>{
        event.preventDefault()
        const formData = new FormData(formTag)
        const json = JSON.stringify(Object.fromEntries(formData))
        
        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = {
            method: "post",
            body: json,
            headers: {
                'Content-Type': 'application/json'
            }
        }
        
    })
})
