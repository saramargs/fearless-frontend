window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/conferences/';
  
    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        console.log("Bad Response", response)
        // Figure out what to do when the response is bad
      } else {
        const data = await response.json();
  
        const conference = data.conferences[0];
        console.log(conference);
  
      }
    } catch (e) {
        console.log("error:", error)
      // Figure out what to do if an error is raised
    }
  
  });