window.addEventListener('DOMContentLoaded', async () =>{
    const stateURL = 'http://localhost:8000/api/states/'

    try {
        const response = await fetch(stateURL)
        const stateSelect = document.getElementById('state')
        
        if (!response.ok) {
            throw new Error("Bad response fromAPI")
        } else {
            const data = await response.json()
            console.log(data)
            for (const state of data){
                const option = document.createElement('option')
                option.innerHTML = state.name
                stateSelect.appendChild(option)

            }
        }
    } catch (e) {
        console.error(e)

    }
})