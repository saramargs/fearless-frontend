
function App(props) {
  return (
    <div>
      Number of attendees: {props.attendees.length}
    </div>
  );
}

export default App;
