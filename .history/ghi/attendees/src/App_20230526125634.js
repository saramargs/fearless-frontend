
function App(props) {
  if (props.attendees === undefined){
    return null
  } 
  return (
    <div>
      Number of attendees: {props.attendees.length}
    </div>
  );
}

export default App;
